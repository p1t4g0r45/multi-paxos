package pl.edu.agh.paxos.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.edu.agh.paxos.domain.Message;

/**
 * Created by Patryk on 2017-11-06.
 */
@Service
public class CommunicationService {

	@Value("${nodes.endpoints}")
	private String[] nodeUrls;
	private RestTemplate restTemplate = new RestTemplate();

	public void post(Message message) {
		for (String url : nodeUrls) {
			restTemplate.postForObject(url, message, Message.class);
		}
	}
}
