package pl.edu.agh.paxos.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.agh.paxos.domain.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
public class ProposerService {
    @Autowired
    private CommunicationService communicationService;
	@org.springframework.beans.factory.annotation.Value("${nodes.index}")
	private int nodeIndex; // should be unique for each app!
	@org.springframework.beans.factory.annotation.Value("${nodes.amount}")
    private int nodesCount;
    private boolean isLeader;
    private int proposalIndex;
    private int proposalNumber;
    private List<Promise> promises = new ArrayList<>();
    private int acceptedCount = 0;
    private Value proposedValue;
    private Value acceptedValue;

    public void onPropose(Propose propose) {
        System.out.println(propose);
        init();
        proposedValue = propose.getValue();
        Message message = isLeader ? new Accept(proposalNumber, propose.getValue()) :
                new Prepare(proposalNumber);
        communicationService.post(message);
    }

    private void init() {
        nextProposalNumber();
        acceptedCount = 0;
        promises.clear();
    }

    private void nextProposalNumber() {
        if (!isLeader) proposalIndex++;
        proposalNumber = proposalIndex * nodesCount + nodeIndex;
    }

    public void onPromise(Promise promise) {
        System.out.println(promise);
        if (promise.getN() != proposalNumber) return;
        promises.add(promise);
        if (promises.size() == (int) Math.ceil((nodesCount + 1) / 2.0)) {
            isLeader = true;
            Promise maxPromise = promises.stream().max(Comparator.comparing(Promise::getN)).get();
            acceptedValue = maxPromise.getValue() != null ? maxPromise.getValue() : proposedValue;
            Accept accept = new Accept(maxPromise.getN(), acceptedValue);
            communicationService.post(accept);
        }
    }

    public void onAccepted(Accepted accepted) {
        System.out.println(accepted);
        if (accepted.getN() != proposalNumber) return;
        acceptedCount++;
        if (acceptedCount == (int) Math.ceil(nodesCount + 1) / 2.0) {
            communicationService.post(new Decide(acceptedValue));
        }
    }

    public void onNack(Nack nack) {
        System.out.println(nack);
        if (nack.getN() != proposalNumber) return;
        isLeader = false;
        proposalNumber = -1;
    }
}
