package pl.edu.agh.paxos.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.agh.paxos.domain.*;


@Service
public class AcceptorService {
    @Autowired
    private CommunicationService communicationService;
    private int minProposalN;
    private int acceptedN;
    private Value acceptedValue;

    public void onPrepare(Prepare prepare) {
        System.out.println(prepare);
        Message message;
        if (prepare.getN() > minProposalN) {
            minProposalN = prepare.getN();
            message = new Promise(minProposalN, acceptedN, acceptedValue);
        } else {
            message = new Nack(prepare.getN());
        }
        communicationService.post(message);
    }

    public void onAccept(Accept accept) {
        System.out.println(accept);
        Message message;
        if (accept.getN() >= minProposalN) {
            minProposalN = accept.getN();
            acceptedN = accept.getN();
            acceptedValue = accept.getValue();
            message = new Accepted(minProposalN);
        } else {
            message = new Nack(accept.getN());
        }
        communicationService.post(message);
    }

}
