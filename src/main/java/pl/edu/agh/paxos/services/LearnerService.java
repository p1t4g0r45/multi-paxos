package pl.edu.agh.paxos.services;

import org.springframework.stereotype.Service;
import pl.edu.agh.paxos.domain.Decide;
import pl.edu.agh.paxos.domain.Value;

import java.util.HashMap;
import java.util.Map;


@Service
public class LearnerService {

    private Map<String, String> keyValueDB = new HashMap<>();

    public void onDecide(Decide decide) {
        System.out.println(decide);
        Value value = decide.getValue();
        keyValueDB.put(value.getKey(), value.getValue());
    }

    public String getValue(String key) {
        return keyValueDB.get(key);
    }

}
