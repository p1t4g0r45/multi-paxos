package pl.edu.agh.paxos.domain;


public class Nack extends Message {
    private int n;

    public Nack() {
    }

    public Nack(int n) {
        this.n = n;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }


    @Override
    public String toString() {
        return "Nack (n = " + n + ")";
    }
}
