package pl.edu.agh.paxos.domain;


public class Promise extends Message {
    private int n;
    private int highestAcceptedN;
    private Value value;

    public Promise() {
    }

    public Promise(int n, int highestAcceptedN, Value value) {
        this.n = n;
        this.highestAcceptedN = highestAcceptedN;
        this.value = value;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public int getHighestAcceptedN() {
        return highestAcceptedN;
    }

    public void setHighestAcceptedN(int highestAcceptedN) {
        this.highestAcceptedN = highestAcceptedN;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Accept (n = " + n + ", highestAcceptedN = " + highestAcceptedN + ", value = " + value + ")";
    }
}
