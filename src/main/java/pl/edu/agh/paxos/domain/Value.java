package pl.edu.agh.paxos.domain;

/**
 * Created by Patryk on 2017-11-05.
 */
public class Value {
	private String key;
	private String value;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
