package pl.edu.agh.paxos.domain;


public class Prepare extends Message {
    private int n;

    public Prepare(int n) {
        this.n = n;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }


    @Override
    public String toString() {
        return "Prepare (n = " + n + ")";
    }
}
