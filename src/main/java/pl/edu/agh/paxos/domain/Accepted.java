package pl.edu.agh.paxos.domain;


public class Accepted extends Message {
    private int n;

    public Accepted() {
    }

    public Accepted(int n) {
        this.n = n;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    @Override
    public String toString() {
        return "Accepted (n = " + n + ")";
    }
}
