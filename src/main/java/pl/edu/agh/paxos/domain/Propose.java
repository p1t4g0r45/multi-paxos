package pl.edu.agh.paxos.domain;


public class Propose extends Message {
    private Value value;

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }


    @Override
    public String toString() {
        return "Propose (value = " + value + ")";
    }
}
