package pl.edu.agh.paxos.domain;

/**
 * Created by baran on 29.10.17.
 */
public class LastSavedValue {
    private Value value;

    public LastSavedValue() {
    }

    public LastSavedValue(Value value) {
        this.value = value;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }
}
