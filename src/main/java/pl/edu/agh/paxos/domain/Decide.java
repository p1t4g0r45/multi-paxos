package pl.edu.agh.paxos.domain;


public class Decide extends Message {
    private Value value;

    public Decide(Value value) {
        this.value = value;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Decide (value = " + value + ")";
    }
}
