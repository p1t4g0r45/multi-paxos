package pl.edu.agh.paxos.domain;


public class Accept extends Message {
    private int n;
    private Value value;

    public Accept(int n, Value value) {
        this.n = n;
        this.value = value;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Accept (n = " + n + ", value = " + value + ")";
    }
}
