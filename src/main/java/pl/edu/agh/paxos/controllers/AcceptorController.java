package pl.edu.agh.paxos.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.edu.agh.paxos.domain.Accept;
import pl.edu.agh.paxos.domain.Prepare;
import pl.edu.agh.paxos.services.AcceptorService;


@Controller
public class AcceptorController {

    @Autowired
    private AcceptorService acceptorService;

    @RequestMapping(value = "/prepare", method = RequestMethod.POST)
    ResponseEntity<HttpStatus> onPrepare(@RequestBody Prepare prepare) {
        acceptorService.onPrepare(prepare);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/accept", method = RequestMethod.POST)
    ResponseEntity<HttpStatus> onAccept(@RequestBody Accept accept) {
        acceptorService.onAccept(accept);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
