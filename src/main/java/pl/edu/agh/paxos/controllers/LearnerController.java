package pl.edu.agh.paxos.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.edu.agh.paxos.domain.Decide;
import pl.edu.agh.paxos.domain.LastSavedValue;
import pl.edu.agh.paxos.services.LearnerService;


@Controller
public class LearnerController {

    @Autowired
    private LearnerService learnerService;

    @RequestMapping(value = "/decide", method = RequestMethod.POST)
    ResponseEntity<HttpStatus> onDecide(@RequestBody Decide decide) {
        learnerService.onDecide(decide);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    @ResponseBody
    LastSavedValue getLastSavedValue() {
        return new LastSavedValue(learnerService.getLastSavedValue());
    }
}
