package pl.edu.agh.paxos.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.edu.agh.paxos.domain.Accepted;
import pl.edu.agh.paxos.domain.Nack;
import pl.edu.agh.paxos.domain.Promise;
import pl.edu.agh.paxos.domain.Propose;
import pl.edu.agh.paxos.services.ProposerService;


@Controller
public class ProposerController {

    @Autowired
    private ProposerService proposerService;

    @RequestMapping(value = "/propose", method = RequestMethod.POST)
    ResponseEntity<HttpStatus> onPropose(@RequestBody Propose propose) {
        proposerService.onPropose(propose);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/promise", method = RequestMethod.POST)
    ResponseEntity<HttpStatus> onPromise(@RequestBody Promise promise) {
        proposerService.onPromise(promise);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/accepted", method = RequestMethod.POST)
    ResponseEntity<HttpStatus> onAccepted(@RequestBody Accepted accepted) {
        proposerService.onAccepted(accepted);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/nack", method = RequestMethod.POST)
    ResponseEntity<HttpStatus> onNack(@RequestBody Nack nack) {
        proposerService.onNack(nack);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
